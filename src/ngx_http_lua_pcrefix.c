// 由 lua_nginx_module 自己来管理 PCRE 内存池
// 这里用到了 pcre 导出的 pcre_malloc 和 pcre_free 两个接口．见
// http://www.pcre.org/original/doc/html/pcreapi.html
// 之前自己在 hack 某个 pcre 相关的功能时，一直遇到内存问题，折腾了大半天都搞不定。
// 后来在网上辗转找到一份资料，提示调用 pcre 时，需要确保在 nginx 内存池中分配。
// 因此要加入下面的保护机制：
/**
    old_pool = ngx_http_lua_pcre_malloc_init(pool);
    rc = func_that_call_pcre(...);
    ngx_http_lua_pcre_malloc_done(old_pool);
**/

/*
 * Copyright (C) Xiaozhe Wang (chaoslawful)
 * Copyright (C) Yichun Zhang (agentzh)
 */


#ifndef DDEBUG
#define DDEBUG 0
#endif
#include "ddebug.h"


#include "ngx_http_lua_pcrefix.h"
#include "stdio.h"

#if (NGX_PCRE)

static ngx_pool_t *ngx_http_lua_pcre_pool = NULL;

static void *(*old_pcre_malloc)(size_t);
static void (*old_pcre_free)(void *ptr);


/* XXX: work-around to nginx regex subsystem, must init a memory pool
 * to use PCRE functions. As PCRE still has memory-leaking problems,
 * and nginx overwrote pcre_malloc/free hooks with its own static
 * functions, so nobody else can reuse nginx regex subsystem... */
static void *
ngx_http_lua_pcre_malloc(size_t size)
{
    dd("lua pcre pool is %p", ngx_http_lua_pcre_pool);

    if (ngx_http_lua_pcre_pool) {
        return ngx_palloc(ngx_http_lua_pcre_pool, size);
    }

    fprintf(stderr, "error: lua pcre malloc failed due to empty pcre pool");

    return NULL;
}


static void
ngx_http_lua_pcre_free(void *ptr)
{
    dd("lua pcre pool is %p", ngx_http_lua_pcre_pool);

    if (ngx_http_lua_pcre_pool) {
        ngx_pfree(ngx_http_lua_pcre_pool, ptr);
        return;
    }

    fprintf(stderr, "error: lua pcre free failed due to empty pcre pool");
}


ngx_pool_t *
ngx_http_lua_pcre_malloc_init(ngx_pool_t *pool)
{
    ngx_pool_t          *old_pool;

    if (pcre_malloc != ngx_http_lua_pcre_malloc) {

        dd("overriding nginx pcre malloc and free");

        ngx_http_lua_pcre_pool = pool;

        old_pcre_malloc = pcre_malloc;
        old_pcre_free = pcre_free;

        pcre_malloc = ngx_http_lua_pcre_malloc;
        pcre_free = ngx_http_lua_pcre_free;

        return NULL;
    }

    dd("lua pcre pool was %p", ngx_http_lua_pcre_pool);

    old_pool = ngx_http_lua_pcre_pool;
    ngx_http_lua_pcre_pool = pool;

    dd("lua pcre pool is %p", ngx_http_lua_pcre_pool);

    return old_pool;
}


void
ngx_http_lua_pcre_malloc_done(ngx_pool_t *old_pool)
{
    dd("lua pcre pool was %p", ngx_http_lua_pcre_pool);

    ngx_http_lua_pcre_pool = old_pool;

    dd("lua pcre pool is %p", ngx_http_lua_pcre_pool);

    if (old_pool == NULL) {
        pcre_malloc = old_pcre_malloc;
        pcre_free = old_pcre_free;
    }
}

#endif /* NGX_PCRE */

/* vi:set ft=c ts=4 sw=4 et fdm=marker: */
???END
