#!/usr/bin/env ruby
# encoding: UTF-8

don_t_read_files = [
  'src/ngx_http_lua_lex.c'
]

has_read_files = IO.popen("git diff --numstat archive | awk '{print $3}'").readlines.map do |f|
  f.strip
end
to_read_files = IO.popen("git ls-files | grep '^src/' | grep '.*\\.c$'").readlines
to_read_file_num = 0
has_read_file_num = 0
has_read_lines = 0
to_read_lines = 0
to_read_files.each do |f|
  f.strip!
  lines = IO.readlines(f).length
  if has_read_files.include?(f)
    puts "\e[32;1m#{f} OK #{lines}\e[0m"
    has_read_file_num += 1
    has_read_lines += lines
  else
    if !don_t_read_files.any? {|prefix| f.start_with? prefix }
      puts "\e[31;1m#{f} TODO #{lines}\e[0m"
      to_read_file_num += 1
      to_read_lines += lines
    end
  end
end

puts "Done: #{has_read_file_num} TODO: #{to_read_file_num}"
puts "Done: #{has_read_lines} TODO: #{to_read_lines}"
